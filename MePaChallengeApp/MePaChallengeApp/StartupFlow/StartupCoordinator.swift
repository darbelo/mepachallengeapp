//
// MainFlowController.swift
//
// This is our 'Startup' (or Main) Coordinator.
//
// This class has the job of setting up the initial navigation and directing the
// user into whatever subflows are appropiate.
// However this app is trivial and only implements the 'Payments' flow, so we'll
// simply start that flow and evntually present the results.
//

import UIKit

enum ApplicationFlows {
    case payments
}

protocol Coordinator {
    associatedtype RootViewControllerType: UIViewController
    func start() -> RootViewControllerType
}

class StartupCoordinator: Coordinator {
    typealias RootViewControllerType = StartViewController

    let window: UIWindow
    let service: MercadoPagoService
    let startViewController: StartViewController
    let paymentsCoordinator: PaymentsFlowCoordinator

    init(window: UIWindow, startViewController: StartViewController, service:MercadoPagoService) {
        self.window = window
        self.service = service
        self.startViewController = startViewController
        self.paymentsCoordinator = PaymentsFlowCoordinator(service:service)
        configure(startViewController: startViewController)
    }

    func start() -> RootViewControllerType {
        startViewController.startDelayedFlowSelection()
        return startViewController
    }

    func configure(startViewController: StartViewController) {
        startViewController.onFlowSelected = { [weak self] (flow: ApplicationFlows) in
            switch flow {
            case .payments:
                self?.startPaymentsFlow()
            }
        }
    }

    func startPaymentsFlow() {
        let viewController = paymentsCoordinator.start()
        startViewController.show(viewController, sender: nil)
    }
}
