//
//  ViewController.swift
//

import UIKit

class StartViewController: UIViewController {
    // This is just a dumb container ViewController, it exists simply to animate
    // a few things during startup and then stay out of the way.

    private let userChoiceDelay = 0.5

    var onFlowSelected: ((ApplicationFlows) -> Void)?

    @IBOutlet var finalConstraints: [NSLayoutConstraint]!
    @IBOutlet var startingConstraints: [NSLayoutConstraint]!
    @IBOutlet var containerView: UIView!
    @IBOutlet var logoImageView: UIImageView!
    private var containedViewController: UIViewController?

    func startDelayedFlowSelection() {
        // Normally, we would have some options and the user would make a choice
        // to go into one of several different flows.  But this is a demo app so
        // we'll wait a little and then fake the only choice available.
        let deadline = DispatchTime.now() + userChoiceDelay
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.onFlowSelected?(.payments)
        }
    }

    override func show(_ vc: UIViewController, sender: Any?) {
        let fullDuration = 0.7

        if let oldViewController = self.containedViewController {
            moveOut(viewController:oldViewController, duration: fullDuration/3) {
                self.moveIn(viewController: vc, duration: fullDuration*2/3)
            }
        }
        else {
            moveIn(viewController: vc, duration: fullDuration)
        }

        self.containedViewController = vc
    }

    func moveIn(viewController: UIViewController?, duration: TimeInterval) {
        guard let viewController = viewController else { return }

        self.view.layoutIfNeeded()
        self.finalConstraints.forEach { (constraint) in
            constraint.priority = UILayoutPriorityDefaultHigh
        }
        self.startingConstraints.forEach { (constraint) in
            constraint.priority = UILayoutPriorityDefaultLow
        }

        addChildViewController(viewController)
        viewController.view.frame = containerView.bounds
        containerView.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)

        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }

    func moveOut(viewController: UIViewController, duration: TimeInterval, completion:@escaping ()->()) {
        self.startingConstraints.forEach { (constraint) in
            constraint.priority = UILayoutPriorityDefaultHigh
        }
        self.finalConstraints.forEach { (constraint) in
            constraint.priority = UILayoutPriorityDefaultLow
        }
        UIView.animate(withDuration: duration, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (Bool) in
            viewController.willMove(toParentViewController: nil)
            viewController.view.removeFromSuperview()
            viewController.removeFromParentViewController()
            completion()
        })
    }
}

