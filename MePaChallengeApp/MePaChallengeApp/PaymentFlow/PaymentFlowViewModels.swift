//
//  PaymentFlowViewModels.swift
//

import Foundation

struct CreditCardViewModel {
    let model: CreditCard
    var name: String {
        return model.name
    }
    var thumbnailURL: URL {
        return model.secureThumbnail
    }

    init(creditCard: CreditCard) {
        model = creditCard
    }
}

struct BankViewModel {
    let model: Bank
    var name: String {
        return model.name
    }
    var thumbnailURL: URL {
        return model.secureThumbnail
    }

    init(bank:Bank) {
        model = bank
    }
}

struct InstallmentsViewModel {
    let model: InstallmentsOptions
    let bank: BankViewModel
    let costs: [CostsViewModel]

    init(installments: InstallmentsOptions) {
        model = installments
        bank = BankViewModel(bank: installments.bank)
        costs = model.costs.map(CostsViewModel.init(costs:))
    }
}

struct CostsViewModel {
    let model: Costs

    var formattedDescription: String {
        let n = Int(model.installments)
        let amount = currencyFormatter.string(from: model.installmentAmount)
        let total = currencyFormatter.string(from: model.totalAmount)
        let format = "%d cuotas de %@ (total %@)"
        return String.localizedStringWithFormat(format, n, amount, total)
    }
    var recommendedMessage: String {
        return model.recommendedMessage
    }

    init(costs: Costs) {
        model = costs
    }
}

// MARK: Formatting helpers

fileprivate let currencyFormatter : NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    return formatter
}()

private extension NumberFormatter {

    func string(from double: Double) -> String {
        return string(from: NSNumber(value:double)) ?? "??"
    }

}
