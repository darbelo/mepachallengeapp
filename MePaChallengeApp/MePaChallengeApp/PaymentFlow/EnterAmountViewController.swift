//
//  EnterAmountViewController.swift
//

import UIKit

class EnterAmountViewController: UIViewController {
    var onAmountEntered : ((String) -> Void)?

    @IBOutlet
    private var amountTextField: UITextField!

    @IBAction
    private func backgroundTouched(_ sender: Any) {
        self.view.endEditing(true)
    }

    @IBAction
    private func confirmButtonTouched(_ sender: Any) {
        if let amount = self.amountTextField.text, amount.isValidAmount {
            onAmountEntered?(amount)
        }
        else {
            doRedStrobeAnimation(with: self.amountTextField)
        }
    }

    func doRedStrobeAnimation(with view: UIView) {
        let originalBakcgroundColor = view.backgroundColor
        let originalTransform = view.transform

        UIView.animate(withDuration: 0.1, animations: {
            view.backgroundColor = .red;
            view.transform = originalTransform.scaledBy(x: 1.2, y: 1.2)
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.1, animations: {
                view.backgroundColor = originalBakcgroundColor
                view.transform = originalTransform
            })
        })
    }

}

extension EnterAmountViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        guard let text = textField.text, !text.isValidAmount else { return }
        doRedStrobeAnimation(with: textField)
    }
}

private extension String {
    // TODO: This should be an injected class that decides what strings we treat
    // as valid amounts, but this is convenient enough right now.
    static var decimalFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter
    }()

    var isValidAmount : Bool {
        return String.decimalFormatter.number(from: self) != nil
    }
}
