//
//  SelectBankViewController.swift
//

import UIKit

class SelectBankViewController: ElementSelectionViewController {

    var onBankSelected: ((BankViewModel)->Void)?
    override var viewModels: [Any] {
        return banks as [Any]
    }
    var banks = [BankViewModel]() {
        didSet {
            tableView?.reloadData()
            updateLoadingIndicator(false)
        }
    }

    override func configure(cell: UITableViewCell, with viewModel: Any) {
        guard let viewModel = viewModel as? BankViewModel else { return }
        cell.textLabel?.text = viewModel.name
    }

    override func didSelect(viewModel:Any) {
        guard let viewModel = viewModel as? BankViewModel else { return }
        onBankSelected?(viewModel)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
}
