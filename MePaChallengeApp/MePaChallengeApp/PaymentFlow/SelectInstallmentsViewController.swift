//
//  SelectInstallmentsViewController.swift
//

import UIKit

class SelectInstallmentsViewController: ElementSelectionViewController {

    var information: InstallmentsViewModel?  {
        didSet {
            tableView?.reloadData()
            updateLoadingIndicator(false)
        }
    }

    var onInstallmentsSelected: ((CostsViewModel)->Void)?

    override var viewModels: [Any] {
        return information?.costs ?? []
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }

    override func configure(cell: UITableViewCell, with viewModel: Any) {
        guard let viewModel = viewModel as? CostsViewModel else { return }
        cell.textLabel?.text = viewModel.formattedDescription
        cell.detailTextLabel?.text = viewModel.recommendedMessage
    }

    override func didSelect(viewModel:Any) {
        guard let viewModel = viewModel as? CostsViewModel else { return }
        onInstallmentsSelected?(viewModel)
    }
}
