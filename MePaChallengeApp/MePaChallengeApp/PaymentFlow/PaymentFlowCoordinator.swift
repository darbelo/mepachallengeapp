//
//  PaymentsFlowCoordinator.swift
//

import UIKit

class PaymentsFlowCoordinator: Coordinator {
    typealias RootViewControllerType = UINavigationController

    let service: MercadoPagoService
    var navigationController = { () -> UINavigationController in
        let navigationController = UINavigationController()
        return navigationController
    }()

    init(service: MercadoPagoService) {
        self.service = service
    }

    func start() -> RootViewControllerType {
        let enterAmountVC = EnterAmountViewController()
        configure(enterAmountViewController: enterAmountVC)
        navigationController.setViewControllers([enterAmountVC], animated: false)
        return navigationController
    }

    func goSelectPaymentMethod(amount: String) {
        let selectCardVC = SelectCreditCardViewController()
        configure(selectCreditCardViewController: selectCardVC, amount:amount)
        navigationController.pushViewController(selectCardVC, animated: true)
        service.fetchActiveCreditCards(
            onSuccess: { (_ cards:[CreditCard]) in
                selectCardVC.creditCards = cards.map {
                    CreditCardViewModel(creditCard:$0)
                }
        },
            onError: { [weak self] (_ error: Error) in
                self?.goErrorAlert()
        })
    }

    func goSelectBank(amount: String, card:CreditCard) {
        let selectBankVC = SelectBankViewController()
        configure(selectBankViewController: selectBankVC, amount:amount, card:card)
        navigationController.pushViewController(selectBankVC, animated: true)
        service.fetchBanks(cardId: card.id,
                           onSuccess: { [weak self] (banks:[Bank]) in
                            if (banks.count > 0) {
                                selectBankVC.banks = banks.map(BankViewModel.init(bank:))
                            }
                            else {
                                // No bankId to re-pick. Remove the VC from the back stack.
                                self?.navigationController.popViewController(animated:false)
                                self?.goSelectInstallments(amount:amount, card:card, bank:nil)
                            }
            },
                           onError: { [weak self] (_ error: Error) in
                            self?.goErrorAlert()
        })
    }

    func goSelectInstallments(amount:String, card:CreditCard, bank:Bank?) {
        let selectInstallmentsVC = SelectInstallmentsViewController()
        configure(selectInstallmentsViewController: selectInstallmentsVC, amount:amount, card:card, bank:bank)
        navigationController.pushViewController(selectInstallmentsVC, animated: true)
        service.fetchInstallments(amount:amount, cardId:card.id, bankId:bank?.id,
                                  onSuccess: { [weak self] (installments: [InstallmentsOptions]) in
                                    guard let info = installments.first else {
                                        self?.goErrorAlert()
                                        return
                                    }
                                    selectInstallmentsVC.information = InstallmentsViewModel(installments:info)
            },
                                  onError: { [weak self] (_ error: Error) in
                                    self?.goErrorAlert()
        })
    }

    func goEndAlert(amount:String, card:CreditCard, bank:Bank?, costs:Costs) {
        let presentingVC = self.navigationController as UIViewController
        let alert = UIAlertController(title: "Eaeaeaea", message: "Saraza", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { (_) in
            presentingVC.dismiss(animated: true)
        })
        navigationController.popToRootViewController(animated: true)
        presentingVC.present(alert, animated: true)
    }

    func goErrorAlert() {
        let presentingVC = self.navigationController as UIViewController
        let alert = UIAlertController(title: "Error",
                                      message: "Se ha producido un error en la carga",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { (_) in
            presentingVC.dismiss(animated: true)
        })
        navigationController.popViewController(animated: true)
        presentingVC.present(alert, animated: true)
    }

    private func configure(enterAmountViewController: EnterAmountViewController) {
        enterAmountViewController.edgesForExtendedLayout = []
        enterAmountViewController.onAmountEntered = { [weak self] (amount) in
            self?.goSelectPaymentMethod(amount:amount)
        }
    }

    private func configure(selectCreditCardViewController: SelectCreditCardViewController, amount: String) {
        selectCreditCardViewController.edgesForExtendedLayout = []
        selectCreditCardViewController.onCreditCardSelected = { [weak self] (card: CreditCardViewModel) in
            self?.goSelectBank(amount: amount, card: card.model)
        }
    }

    private func configure(selectBankViewController:SelectBankViewController, amount: String, card:CreditCard) {
        selectBankViewController.edgesForExtendedLayout = []
        selectBankViewController.onBankSelected = { [weak self] (bank: BankViewModel) in
            self?.goSelectInstallments(amount: amount, card: card, bank: bank.model)
        }
    }

    private func configure(selectInstallmentsViewController:SelectInstallmentsViewController, amount:String, card:CreditCard, bank:Bank?) {
        selectInstallmentsViewController.edgesForExtendedLayout = []
        selectInstallmentsViewController.onInstallmentsSelected = { [weak self] (selected: CostsViewModel) in
            self?.goEndAlert(amount: amount, card: card, bank: bank, costs: selected.model)
        }
    }
}
