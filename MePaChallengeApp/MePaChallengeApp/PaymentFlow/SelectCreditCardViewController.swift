//
//  SelectPaymentMethodViewController.swift
//

import UIKit

class SelectCreditCardViewController: ElementSelectionViewController {

    var onCreditCardSelected: ((CreditCardViewModel)->())?
    override var viewModels: [Any] {
        return creditCards as [Any]
    }
    var creditCards = [CreditCardViewModel]() {
        didSet {
            tableView?.reloadData()
            updateLoadingIndicator(false)
        }
    }

    override func configure(cell: UITableViewCell, with viewModel: Any) {
        guard let viewModel = viewModel as? CreditCardViewModel else { return }
        cell.textLabel?.text = viewModel.name
    }

    override func didSelect(viewModel:Any) {
        guard let viewModel = viewModel as? CreditCardViewModel else { return }
        onCreditCardSelected?(viewModel)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }

}
