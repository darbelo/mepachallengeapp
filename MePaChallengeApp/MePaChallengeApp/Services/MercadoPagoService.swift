//
//  MercadoPagoService.swift
//
// This wrapper tries to keep the application insulated from the MercadoPagoAPI.
// This is not a big deal in an app this simple. So we'll just wrap some structs
// or hide them behind trivially-mapped protocols.
// This way the app can depend on the types we declare here and we have a single
// place where we can add a translation layer if the MP types ever change.
//

import UIKit
import MercadoPagoAPI

class MercadoPagoService {
    private let apiClient: ApiClient

    init(apiClient:ApiClient) {
        self.apiClient = apiClient
    }

    func fetchActiveCreditCards(onSuccess:@escaping ([CreditCard])->Void, onError:@escaping (Error)->Void) {
        apiClient.fetchPaymentMethods(
            onSuccess: { (methods:[PaymentMethod]) in
                // We want the active cards, not all the payment types.
                let cards : [CreditCard] = methods.filter {
                    $0.paymentType == .creditCard && $0.status == .active
                }
                onSuccess(cards)
            },
            onError: onError)
    }

    func fetchBanks(cardId:String, onSuccess:@escaping([Bank])->Void, onError:@escaping(Error)->Void) {
        apiClient.fetchCardIssuers(paymentMethodId: cardId,
                                   onSuccess: { (issuers:[CardIssuer]) in
                                    onSuccess(issuers as [Bank])
        },
                                   onError:onError)

    }

    func fetchInstallments(amount: String, cardId: String, bankId: String?, onSuccess:@escaping([InstallmentsOptions])->Void, onError:@escaping(Error)->Void) {
        apiClient.fetchInstallments(amount:amount,
                                    paymentMethodId:cardId,
                                    issuerId:bankId,
                                    onSuccess:{ (installments: [Installments]) in
                                        onSuccess(installments as [InstallmentsOptions])
        },
                                    onError:onError)
    }


}

// Subset of the Payment Method struct. 
// This way we can save a map() call and just cast it to the protocol and still
// keep the app insulated from the MP types.
protocol CreditCard {
    var id: String { get }
    var name: String { get }
    var secureThumbnail: URL { get }
    var thumbnail: URL { get }
    var minimumAllowedAmount: Double { get }
    var maximumAllowedAmount: Double { get }
}

protocol Bank {
    var id: String { get }
    var name: String { get }
    var secureThumbnail: URL { get }
}

protocol InstallmentsOptions {
    var paymentMethodId: String { get }
    var bank: Bank { get }
    var costs: [Costs] { get }
}

protocol Costs {
    var installments: Double { get }
    var interestRate: Double { get }
    var labels: [String] { get }
    var minimumAllowedAmount: Double { get }
    var maximumAllowedAmount: Double { get }
    var recommendedMessage: String { get }
    var installmentAmount: Double { get }
    var totalAmount: Double { get }
}

// Trivial conformances.
extension PaymentMethod : CreditCard {}
extension CardIssuer : Bank {}
extension Installments.PayerCosts : Costs {}

// Amost trivial conformance :)
extension Installments : InstallmentsOptions {
    var bank: Bank {
        return issuer as Bank
    }
    var costs: [Costs] {
        return self.payerCosts as [Costs]
    }
}
