//
//  AppDelegate.swift
//

import UIKit
import MercadoPagoAPI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: StartupCoordinator?
    var service: MercadoPagoService = {
        let publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"
        let apiClient = ApiClient.makeApiClient(apiKey: publicKey)
        return MercadoPagoService(apiClient:apiClient)
    } ()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let startingViewController = window!.rootViewController as! StartViewController
        coordinator = StartupCoordinator(window: window!, startViewController: startingViewController, service:service)
        _ = coordinator?.start()
        return true
    }
}

