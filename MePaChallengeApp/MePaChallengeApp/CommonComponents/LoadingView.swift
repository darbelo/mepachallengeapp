//
//  LoadingView.swift
//

import UIKit

class LoadingView: UIView {
    var active: Bool {
        didSet {
            if (active) {
                activityIndicatorView?.startAnimating()
            }
            else {
                activityIndicatorView?.stopAnimating()
            }
        }
    }

    @IBOutlet var loadingTextLabel: UILabel?
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView?

    override init(frame: CGRect) {
        active = false
        super.init(frame:frame)

        initSubviewsIfMissing()
    }
    
    required init?(coder aDecoder: NSCoder) {
        active = false
        super.init(coder: aDecoder)
        initSubviewsIfMissing()
    }

    func initSubviewsIfMissing() {
        guard activityIndicatorView == nil, loadingTextLabel == nil else {
            return
        }

        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicatorView = spinner

        addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = NSLocalizedString("Cargando...", comment: "")
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        self.loadingTextLabel = label

        addSubview(label)
        label.centerXAnchor.constraint(equalTo: spinner.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: spinner.topAnchor, constant: -16.0).isActive = true
    }
}
