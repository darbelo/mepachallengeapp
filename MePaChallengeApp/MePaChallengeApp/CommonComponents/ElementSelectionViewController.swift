//
//  ElementSelectionViewController.swift
//

import UIKit

class ElementSelectionViewController: UIViewController {
    @IBOutlet var loadingView: LoadingView?
    @IBOutlet var tableView: UITableView?

    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateLoadingIndicator(animated)
    }

    // MARK: - Template methods for subclasses

    var cellIdentifier : String {
        return "CellIdentifier"
    }
    var viewModels : [Any] {
        fatalError("Subclasses should override this getter")
    }
    func configure(cell: UITableViewCell, with viewModel: Any) {
        // Override in subclass
    }
    func didSelect(viewModel:Any) {
        // Override in subclass
    }

    // MARK: - LoadingView handling

    func updateLoadingIndicator(_ animated: Bool) {
        let animations = {
            let shouldShowTableView = self.viewModels.count > 0
            self.tableView?.alpha = shouldShowTableView ? 1.0: 0.0
            self.loadingView?.alpha = shouldShowTableView ? 0.0: 1.0
            self.loadingView?.active = !shouldShowTableView
        }
        if (animated) {
            UIView.animate(withDuration: 1.0, animations: animations)
        }
        else {
            animations()
        }
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension ElementSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let selected = viewModels[indexPath.row]
        didSelect(viewModel:selected)
    }
}

extension ElementSelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let viewModel = viewModels[indexPath.row]
        configure(cell:cell, with:viewModel)
        return cell
    }
}
