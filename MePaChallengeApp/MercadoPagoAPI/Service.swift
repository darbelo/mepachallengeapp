//
//  Service.swift
//

import Foundation

// MARK - Service protocol.
// Basic interface that we want for our services. There's plenty of room to make
// it more clever, but it works fine as it is now.
public protocol Service {
    associatedtype FetchedType
    
    typealias ResultClosure = (FetchedType) -> Void
    typealias ErrorClosure = (Error) -> Void

    init(url: URL, params: [String:String])

    func fetch(path: String, params: [String:String], onSuccess: @escaping ResultClosure, onError:@escaping ErrorClosure)
}

// MARK - Generic 'JSON' Service
// With just a little bit of 'generics trickery' we can handle all services that
// respond with JSON.  Of course, this offloads all validation to the TargetType
// and DeserializerType implementations and simply passes the errors through the
// callbacks.
// Whoever calls us will need to know the errors that DeserializerType can throw
// at them and handle everything on the 'onError' callback.
class JSONService<DeserializerType : Deserializer>: Service {
    typealias FetchedType = DeserializerType.Target

    private let urlComponents : URLComponents
    private let defaultParams : [String : String]

    var queue = DispatchQueue.main

    required init(url: URL, params: [String : String]) {
        urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        defaultParams = params
    }

    func fetch(path: String, params: [String:String], onSuccess: @escaping ResultClosure, onError: @escaping ErrorClosure) {
        var components = urlComponents
        components.path += path
        var allParams = defaultParams
        params.forEach { allParams[$0] = $1 }
        components.queryItems = allParams.map { URLQueryItem(name: $0, value: $1) }

        let request = URLRequest(url: components.url!)
        let queue = self.queue
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                queue.async { onError(error ?? ServiceError.unknown) }
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data)
                let result : FetchedType = try DeserializerType.from(json:json)
                queue.async { onSuccess(result) }
            } catch let jsonError {
                queue.async { onError(jsonError) }
            }
        }.resume()
    }

    enum ServiceError: Error {
        case unknown
    }
}
