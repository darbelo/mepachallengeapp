//
//  Installments.swift
//

import Foundation

public struct Installments {
    public let paymentMethodId: String
    public let paymentType: PaymentType
    public let issuer: CardIssuer
    public let payerCosts: [PayerCosts]

    public struct PayerCosts {
        public let installments: Double
        public let interestRate: Double
        public let labels: [String]
        public let minimumAllowedAmount: Double
        public let maximumAllowedAmount: Double
        public let recommendedMessage: String
        public let installmentAmount: Double
        public let totalAmount: Double
    }
}

extension Installments: JSONDecodable {
    // GENERATED //
    init(jsonObject: [String : Any]) throws {
        guard let paymentMethodId = jsonObject["payment_method_id"] as? String else { throw JSONDecodingError.missing("payment_method_id") }
        guard let paymentTypeId = jsonObject["payment_type_id"] as? String else { throw JSONDecodingError.missing("payment_type_id") }
        guard let paymentType = PaymentType(rawValue: paymentTypeId) else { throw JSONDecodingError.invalid("payment_type_id", paymentTypeId) }
        guard let issuerObject = jsonObject["issuer"] as? [String:Any] else { throw JSONDecodingError.missing("issuer") }
        guard let issuer = try? CardIssuer(jsonObject: issuerObject) else { throw JSONDecodingError.invalid("issuer", issuerObject) }
        guard let payerCostsObject = jsonObject["payer_costs"] as? [[String:Any]] else { throw JSONDecodingError.missing("payer_costs") }
        guard let payerCosts = try? payerCostsObject.map({ try PayerCosts(jsonObject: $0) }) else { throw JSONDecodingError.invalid("payer_costs", issuerObject) }
        self.init(paymentMethodId: paymentMethodId, paymentType: paymentType, issuer: issuer, payerCosts: payerCosts)
    }
}

extension Installments.PayerCosts: JSONDecodable {
    // GENERATED //
    init(jsonObject: [String : Any]) throws {
        guard let installments = jsonObject["installments"] as? Double else { throw JSONDecodingError.missing("installments") }
        guard let interestRate = jsonObject["installment_rate"] as? Double else { throw JSONDecodingError.missing("installment_rate") }
        guard let labels = jsonObject["labels"] as? [String] else { throw JSONDecodingError.missing("labels") }
        guard let minimumAllowedAmount = jsonObject["min_allowed_amount"] as? Double else { throw JSONDecodingError.missing("min_allowed_amount") }
        guard let maximumAllowedAmount = jsonObject["max_allowed_amount"] as? Double else { throw JSONDecodingError.missing("max_allowed_amount") }
        guard let recommendedMessage = jsonObject["recommended_message"] as? String else { throw JSONDecodingError.missing("recommended_message") }
        guard let installmentAmount = jsonObject["installment_amount"] as? Double else { throw JSONDecodingError.missing("installment_amount") }
        guard let totalAmount = jsonObject["total_amount"] as? Double else { throw JSONDecodingError.missing("total_amount") }
        self.init(installments: installments, interestRate: interestRate, labels: labels, minimumAllowedAmount: minimumAllowedAmount, maximumAllowedAmount: maximumAllowedAmount, recommendedMessage: recommendedMessage, installmentAmount: installmentAmount, totalAmount: totalAmount)
    }
}
