//
//  JSON.swift
//

import Foundation

protocol JSONDecodable {
    init(jsonObject:[String:Any]) throws
}

enum JSONDecodingError: Error {
    case missing(String)
    case invalid(String, Any)
}

protocol Deserializer {
    associatedtype Target
    static func from(json:Any) throws -> Target
}

class JSONCollectionDecoder<T: JSONDecodable> : Deserializer {
    typealias Target = [T]

    static func from(json: Any) throws -> Target {
        guard let jsonArray = json as? [[String:Any]] else {
            throw JSONDecodingError.invalid("<root>", json)
        }
        return try jsonArray.map { try T(jsonObject:$0) }
    }
}

class JSONObjectDecoder<T: JSONDecodable> : Deserializer {
    typealias Target = T

    static func from(json: Any) throws -> Target {
        guard let jsonObject = json as? [String:Any] else {
            throw JSONDecodingError.invalid("<root>", json)
        }

        return try T(jsonObject:jsonObject)
    }
}
