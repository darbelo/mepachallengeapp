//
//  PaymentType.swift
//

import Foundation

public enum PaymentType: String {
    case ticket = "ticket"
    case atm = "atm"
    case creditCard = "credit_card"
    case debitCard = "debit_card"
    case prepaidCard = "prepaid_card"
}
