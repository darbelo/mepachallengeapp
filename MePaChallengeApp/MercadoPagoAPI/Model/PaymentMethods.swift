//
//  PaymentMethod.swift
//

import Foundation


public struct PaymentMethod {
    public let id: String
    public let name: String
    public let paymentType: PaymentType
    public let status: Status
    public let secureThumbnail: URL
    public let thumbnail: URL
    public let deferredCapture: DeferredCapture
    public let additionalInfoNeeded: [String]
    public let minimumAllowedAmount: Double
    public let maximumAllowedAmount: Double

    public enum Status: String {
        case active = "active"
        case deactive = "deactive"
        case temporallyDeactive = "temporally_deactive"
    }

    public enum DeferredCapture: String {
        case supported = "supported"
        case unsupported = "unsupported"
        case doesNotApply = "does_not_apply"
    }
}

extension PaymentMethod: JSONDecodable {
    // GENERATED //
    init(jsonObject: [String:Any]) throws {
        guard let id = jsonObject["id"] as? String else { throw JSONDecodingError.missing("id") }
        guard let name = jsonObject["name"] as? String else { throw JSONDecodingError.missing("name") }
        guard let paymentTypeId = jsonObject["payment_type_id"] as? String else { throw JSONDecodingError.missing("payment_type_id") }
        guard let paymentType = PaymentType(rawValue: paymentTypeId) else { throw JSONDecodingError.invalid("payment_type_id", paymentTypeId) }
        guard let statusId = jsonObject["status"] as? String else { throw JSONDecodingError.missing("status") }
        guard let status = Status(rawValue: statusId) else { throw JSONDecodingError.invalid("status", paymentTypeId) }
        guard let secureThumbnailString = jsonObject["secure_thumbnail"] as? String else { throw JSONDecodingError.missing("secure_thumbnail") }
        guard let secureThumbnail = URL(string:secureThumbnailString) else { throw JSONDecodingError.invalid("secure_thumbnail", secureThumbnailString)}
        guard let thumbnailString = jsonObject["thumbnail"] as? String else { throw JSONDecodingError.missing("thumbnail") }
        guard let thumbnail = URL(string:thumbnailString) else { throw JSONDecodingError.invalid("thumbnail", thumbnailString)}
        guard let deferredCaptureId = jsonObject["deferred_capture"] as? String else { throw JSONDecodingError.missing("deferred_capture") }
        guard let deferredCapture = DeferredCapture(rawValue: deferredCaptureId) else { throw JSONDecodingError.invalid("deferred_capture", paymentTypeId) }
        guard let additionalInfoNeeded = jsonObject["additional_info_needed"] as? [String] else { throw JSONDecodingError.missing("additional_info_needed") }
        guard let minimumAllowedAmount = jsonObject["min_allowed_amount"] as? Double else { throw JSONDecodingError.missing("min_allowed_amount") }
        guard let maximumAllowedAmount = jsonObject["max_allowed_amount"] as? Double else { throw JSONDecodingError.missing("max_allowed_amount") }

        self.init(id:id, name:name, paymentType:paymentType, status:status, secureThumbnail:secureThumbnail, thumbnail:thumbnail, deferredCapture:deferredCapture, additionalInfoNeeded:additionalInfoNeeded, minimumAllowedAmount: minimumAllowedAmount, maximumAllowedAmount: maximumAllowedAmount)
    }
}
