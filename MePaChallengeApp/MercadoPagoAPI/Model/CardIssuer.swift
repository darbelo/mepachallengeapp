//
//  CardIssuer.swift
//

import Foundation

public struct CardIssuer {
    public let id : String
    public let name: String
    public let secureThumbnail: URL
    public let thumbnail: URL
}

extension CardIssuer : JSONDecodable {
    // GENERATED //
    init(jsonObject: [String:Any]) throws {
        guard let id = jsonObject["id"] as? String else { throw JSONDecodingError.missing("id") }
        guard let name = jsonObject["name"] as? String else { throw JSONDecodingError.missing("name") }
        guard let secureThumbnailString = jsonObject["secure_thumbnail"] as? String else { throw JSONDecodingError.missing("secure_thumbnail") }
        guard let secureThumbnail = URL(string:secureThumbnailString) else { throw JSONDecodingError.invalid("secure_thumbnail", secureThumbnailString)}
        guard let thumbnailString = jsonObject["thumbnail"] as? String else { throw JSONDecodingError.missing("thumbnail") }
        guard let thumbnail = URL(string:thumbnailString) else { throw JSONDecodingError.invalid("thumbnail", secureThumbnailString)}
        self.init(id: id, name:name, secureThumbnail:secureThumbnail, thumbnail:thumbnail)
    }
}
