//
//  MercadoPago.swift
//

import Foundation

let defaultBaseUrl = "https://api.mercadopago.com/v1/"
let defaultPaths = (
    paymentMethods: "payment_methods",
    cardIssuers: "payment_methods/card_issuers",
    installments: "payment_methods/installments"
)

public class ApiClient {
    public static func makeApiClient(apiKey: String, baseUrl: String = defaultBaseUrl) -> ApiClient {
        return ApiClient(apiKey: apiKey, baseUrl: baseUrl)
    }

    public func fetchPaymentMethods(path: String = defaultPaths.paymentMethods, onSuccess: @escaping ([PaymentMethod])->Void, onError: @escaping (Error)->Void) {
        paymentsService.fetch(path:path, params: [:], onSuccess: onSuccess, onError: onError)
    }

    public func fetchCardIssuers(path: String = defaultPaths.cardIssuers, paymentMethodId:String, onSuccess: @escaping ([CardIssuer])->Void, onError: @escaping (Error)->Void) {
        let params = ["payment_method_id" : paymentMethodId]
        issuersService.fetch(path: path, params: params, onSuccess: onSuccess, onError: onError)
    }

    public func fetchInstallments(path: String = defaultPaths.installments, amount:String, paymentMethodId: String, issuerId: String?, onSuccess: @escaping ([Installments])->Void, onError: @escaping (Error)->Void) {
        var params = ["payment_method_id" : paymentMethodId,
                      "amount":amount]
        if let issuerId = issuerId {
            params["issuer.id"] = issuerId
        }
        installmentsService.fetch(path: path, params: params, onSuccess: onSuccess, onError: onError)
    }

    // MARK: -
    typealias PaymentServiceType = JSONService<JSONCollectionDecoder<PaymentMethod>>
    let paymentsService: PaymentServiceType

    typealias IssuersServiceType = JSONService<JSONCollectionDecoder<CardIssuer>>
    let issuersService: IssuersServiceType

    typealias InstallmentsServiceType = JSONService<JSONCollectionDecoder<Installments>>
    let installmentsService: InstallmentsServiceType

    fileprivate init(apiKey: String, baseUrl: String) {
        let rootUrl = URL(string:baseUrl)!

        paymentsService = PaymentServiceType(url:rootUrl, params: ["public_key":apiKey])

        issuersService = IssuersServiceType(url:rootUrl, params: ["public_key":apiKey])

        installmentsService = InstallmentsServiceType(url:rootUrl, params: ["public_key":apiKey])
    }

}
